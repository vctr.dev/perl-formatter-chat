# Perl Formatter Dev Chat

---

## What is it?

- `HVE::Utility::Formatter::format_object`
- A declarative reducer
- `($anything, $schema) -> hashref`
```
A -> Formatter -> A'
         ^
		 |
Formatting instructions
     (schema)
```

+++

## Victor, please show the code

---

## Schema

```
- schema: Hashref
	- <field>: String
		- [predicate = sub { 1 }]: CodeRef->($object, $field)
		- accessor: CodeRef->($object, $field)
		- [formatter = sub { $_[0] }]: CodeRef->($value)
```

+++

### Terms

- `field`: `key` in the output
- `predicate`: Include this key-value pair?
- `accessor`: `value` of the output
- `formatter`: How should the `value` look?

+++

### Example

```perl
# Usage
my $current_user = $some_user;
format_object(
    $some_obj,
    {
        charged => {
            predicate => sub { $current_user->can_view_rate }, 
            accessor  => sub { $_[0]->get('rate_charged') },
            formatter => &format_float,
        }
    }
);
# { charged => 2.0 };
```

---

## Why do we need it

- Change from one format to another
	- e.g. an IRX object to a hash
- Extracting common operations
	- `if`-chains, especially with permission checks
	- Assigning from one value to another
	- Formatting the value (e.g. yes/no to 1/0)

---

## Where is it currently used?

- GraphQL-isque queries (Dynamic structure)
	- Newer API endpoints
		- Through wrapper `HVE::Utility::API::Formatter`
			- `build_payload`
	- Public API
		- Through wrapper `AffinityLive::API::Utilities`
			- `build_payload`

+++

## Examples in the code

```
HVE::Utility::API::Activity::Template::Category::get_api_fields
HVE::Utility::API::Issue::Type::get_api_fields
HVE::API::Page::Timesheet::Day::View::_get_schema
```

+++

"A Foolish Consistency is the Hobgoblin of Little Minds" ~ Ralph Waldo Emerson, Self-Reliance

+++

## When should we not use it?

- When using it would add complexity to the code and make it less readable
	- e.g. when you just need a hash slice, formatter is overkill.
	- e.g. when you just need a hash of typed values, formatter is overkill.

---

## FAQ

- Why do we separate accessor from formatter?
	- Single Responsibility Principle
		- Reusable
		- Testable
- More complex objects
	- Nested object
	- Bulk getting/optimizations

---

### Nested Object

+++

#### Format Object in Accessor
```perl
my $formatted_obj = format_obj(
    $foo,
    {
        bar => {
            accessor => sub {
				format_object( $bar, $schema_for_bar )
			}
        }
    }
);
```

+++

#### Construct First Then Assign

```
$formatted_obj->{baz} = format_object(
	$baz,
	$schema_for_baz
);
```

---

### Bulk getting/optimizations

+++

#### Pass in as closure when constructing schema

```perl
my $eggs = get_eggs; # Bulk get

# Schema
{
    egg => {
        accessor => sub { $eggs->{ $_[0]->get('egg_id') } }
    }
};
```

+++

#### Pass in as params (testable)

```perl
my $eggs = get_eggs;    # Bulk get

format_object(
    { spam => $spam, eggs => $eggs },
    {
        egg => {
            accessor => sub {
				$_[0]->{eggs}->{ $_[0]->{spam}->get('egg_id') }
			}
        }
    }
);
```

---

# Want to know more?

- Higher Order Perl
- Lexical Closure
